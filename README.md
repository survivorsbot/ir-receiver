# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Detailing work done to set up IR Receiver for project.

### How do I get set up? ###

Follow the instructions given by the guideline used below. The code used was given through the SimpleIDE libraries for example use and changed to work with our particular setup.

/*
  Test SIRC Remote.c

  http://learn.parallax.com/propeller-c-simple-devices/ir-receiver-and-remote

  Connect 38 kHz IR receiver to Propeller P10
  Decodes button presses on Sony-compatible IR remote 
  Run with Terminal 
*/

#include "simpletools.h"                      // Libraries included
#include "sirc.h"

int main()                                    // main function
{
  sirc_setTimeout(5000);                      // -1 if no remote code in 5 s

  while(1)                                    // Repeat indefinitely
  {                                         
    int readButton;                              //  decoding signal from P10
    print("%c remote button = %d%c",          //  Display button # decoded
          HOME, sirc_button(10), CLREOL);     //  from signal on P10
   
   
    pause(1000);                               // 1 s before loop repeat
  }  
}
### Contribution guidelines ###
https://learn.parallax.com/tutorials/language/propeller-c/propeller-c-simple-devices/ir-receiver-and-remote



### Who do I talk to? ###

Christopher Hoang